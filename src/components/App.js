import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Button } from 'react-bootstrap';
import Category from './Category';
import FormArticle from './FormArticle';

class App extends Component {
   constructor(props) {
      super(props);
      this.state = {
         artCategories: [],
      };
   }

   componentDidMount() {
      this.getCategories();
   }

   getCategories() {
      fetch("http://localhost:4000/categories")
         .then(res => res.json())
         .then(
            (result) => {
               this.setState({ artCategories: result });
            },
            (error) => {
               console.log("erreur : " + error)
            }
         )
   }

   render() {
      var artCategories = this.state.artCategories;

      return (
         <div className="App" >
            <FormArticle action="add" />
            <Router>
               <div>
                  <span>Catégories : </span>
                  {
                     artCategories.map((artCat, index) => {
                        return <Link key={index} to={"/" + artCat._id}><Button variant="secondary">{artCat.libelle}</Button></Link>
                     })
                  }
                  <Switch>
                     <Route path="/:id" children={<Category />} />
                  </Switch>
               </div>
            </Router>
         </div>
      );
   }
}

export default App;
