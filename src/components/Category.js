import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import Article from './Article';

// Notre composant List va nous servir a afficher chacun des items (Element)
class Category extends Component {
   constructor(props) {
      super(props);
      this.state = {
         idCategory: this.props.match.params.id,
         libelleCat: "",
         articles: []
      };
   }

   componentDidMount() {
      this.getArticles();
   }

   getArticles() {
      fetch("http://localhost:4000/articles/category/" + this.state.idCategory)
         .then(res => res.json())
         .then(
            (result) => {
               this.setState({ articles: result });
            },
            (error) => {
               console.log("erreur : " + error);
            }
         );
      
      fetch("http://localhost:4000/category/" + this.state.idCategory)
         .then(res => res.json())
         .then(
            (result) => {
               this.setState({ libelleCat: result.libelle });
            },
            (error) => {
               console.log("erreur : " + error);
            }
         );
   }

   render() {
      return (
         <div>
            <h2>{this.state.libelleCat}</h2>
            {
               this.state.articles.map((article, index) => {
                  return <Article article={article} key={index}/>
               })
            }
         </div>
      );
   }
}

export default withRouter(Category);