import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import FormArticle from './FormArticle';

// Notre composant Element est là pour gérer le rendu pour chacun des items.
class Article extends Component {
   constructor(props) {
      super(props);
      this.state = {
         article: this.props.article
      }
   }

   deleteArticle() {
      fetch("/article/delete/"+this.state.article._id, {
         method: "DELETE"
      })
   }

   render() {
      var article = this.state.article;
      var tags = article.tags.map(tag => '#' + tag).join(', ');

      return (
         <div id="article">
            <h2>{article.titre}</h2>
            <span className="tags">{tags}</span>
            <div className="content">
               {article.content}
            </div>
            <Button variant="primary" onClick={this.deleteArticle}>
               Supprimer
            </Button>
            <FormArticle action="modify" idArticle={this.state.article._id}/>
         </div>
      );
   }
}

export default Article