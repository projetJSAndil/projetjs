import 'bootstrap/dist/css/bootstrap.min.css';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import React, { Component } from 'react';
import { Button, Modal, Form, Badge } from 'react-bootstrap';
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from 'draftjs-to-html';
import { EditorState, ContentState, convertFromHTML } from 'draft-js';

class FormArticle extends Component {
   constructor(props) {
      super(props);
      this.state = {
         show: false,
         contentState: null,
         action: this.props.action,
         defaultEditorState: "",
         artCategories: [],
         titleButton: null,
         tagsInBase: ""
      };
   }

   componentDidMount() {
      if (this.state.action === "modify") {
         this.setState({ titleButton: "Modifier" });

         var idArticleToModify = this.props.idArticle;
         fetch("http://localhost:4000/article/" + idArticleToModify)
            .then(res => res.json())
            .then(
               (result) => {
                  this.initialiseUpdateForm(result);
               },
               (error) => {
                  console.log("erreur : " + error)
               }
            )

         
      } else if (this.state.action === "add") {
         this.setState({ titleButton: "Ajouter un article" });
      }

      this.getCategories();
   }

   initialiseUpdateForm(article) {
      // ajout du content
      const blocksFromHTML = convertFromHTML(article.content);
      const state = ContentState.createFromBlockArray(
         blocksFromHTML.contentBlocks,
         blocksFromHTML.entityMap
      );
      this.setState({ defaultEditorState: EditorState.createWithContent(state) });

      // ajout de la catégorie
      document.getElementById('category').value = article.libelleCat;
      // ajout du titre
      document.getElementById('title').value = article.title
      // ajout des tags
      var tagsInBase = "";
      article.tags.forEach(tag => {
         tagsInBase += <a href={"http://localhost:4000/category/"+article.IdCat+"/deleteTag/"+tag._id}><Badge pill variant="primary">X {tag.rawname}</Badge></a>
      });
      this.setState({tagsInBase: tagsInBase});
   }

   addUpdateArticle() {
      var formData = {
         'category': document.getElementById("category").value,
         'titre': document.getElementById("title").value,
         'tags': document.getElementById("tags").value.trim().split(','),
         'content': draftToHtml(this.state.contentState)
      }
      formData = JSON.stringify(formData);

      var url = "";
      var method = "";
      if (this.state.action === "modify") {
         url = "http://localhost:4000/article/update/" + this.props.idArticle;
         method = "PUT";
      } else if (this.state.action === "add") {
         url = "http://localhost:4000/article/add";
         method = "POST";
      }

      fetch(url, {
         method: method,
         body: formData
      })
   }

   getCategories() {
      fetch("http://localhost:4000/categories")
         .then(res => res.json())
         .then(
            (result) => {
               this.setState({ artCategories: result });
            },
            (error) => {
               console.log("erreur : " + error);
            }
         )
   }

   render() {
      var artCategories = this.state.artCategories;
      const handleClose = () => this.setState({ show: false });
      const handleShow = () => this.setState({ show: true });
      const saveChanges = () => {
         this.addUpdateArticle();
         handleClose();
      }
      const onContentStateChange = contentState => {
         this.setState({ contentState: contentState });
      }

      return (
         <div>
            <Button variant="primary" onClick={handleShow}>
               {this.state.titleButton}
            </Button>

            <Modal show={this.state.show} onHide={handleClose}>
               <Modal.Header closeButton>
                  <Modal.Title>Ajouter un article</Modal.Title>
               </Modal.Header>
               <Modal.Body>
                  <Form>
                     <Form.Group controlId="category">
                        <Form.Label>Catégorie</Form.Label>
                        <Form.Control as="select">
                           {
                              artCategories.map((cat, index) => {
                                 return <option key={index} id={cat._id}>{cat.libelle}</option>
                              })
                           }
                        </Form.Control>
                     </Form.Group>
                     <Form.Group controlId="title">
                        <Form.Label>Titre</Form.Label>
                        <Form.Control type="text" />
                     </Form.Group>
                     <Form.Group controlId="tags">
                        <div id="tagsInBase">{this.state.tagsInBase}</div>
                        <Form.Label>Tags</Form.Label>
                        <Form.Control type="text" />
                     </Form.Group>
                     <Form.Group controlId="content">
                        <Form.Label>Contenu</Form.Label>
                        <Editor
                           defaultEditorState={this.state.defaultEditorState}
                           onContentStateChange={onContentStateChange}
                        />
                     </Form.Group>
                  </Form>
               </Modal.Body>
               <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                     Fermer
                  </Button>
                  <Button variant="primary" onClick={saveChanges}>
                     Sauvegarder
                  </Button>
               </Modal.Footer>
            </Modal>
         </div>
      );
   }
}

export default FormArticle;
