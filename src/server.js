const { ObjectId, ObjectID } = require('bson');
const mongoose = require('mongoose')
var Schema = mongoose.Schema;
const url = "mongodb+srv://root:root@cluster0.sckoc.mongodb.net/projetjs?retryWrites=true&w=majority";

const connectionParams={
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true 
}
mongoose.connect(url,connectionParams)
  .then( () => {
      console.log('Connected to database ');

      /* -------------- */
      /* --- MODELS --- */
      /* -------------- */

      var ArticleSchema  = new Schema({ 
        contenu: String, 
        titre:String,
        id_categorie: ObjectId,
        tags : [{
          type: mongoose.Schema.Types.ObjectId, 
          ref : "Tag"
        }]}, { collection : 'articles' });

      const Article = mongoose.model('Article', ArticleSchema);

      var CategorieSchema  = new Schema({ libelle: String}, { collection : 'categories' });
      const Categorie = mongoose.model('Categorie', CategorieSchema);

      var TagSchema  = new Schema({ 
        rawname: String,
        articles : [{
          type: mongoose.Schema.Types.ObjectId, 
          ref : "Article"
        }]}, { collection : 'tags' });
      const Tag = mongoose.model('Tag', TagSchema);

      /* ----------------- */
      /* --- FUNCTIONS --- */
      /* ----------------- */

      /**
       * Fonction principale de création de tags / association avec un ou plusieurs articles.
       * @param {ObjectId} articleid 
       * @param {Array} tags 
       * @param {boolean} isUpdate 
       */
      function manageTags(articleid, tags, isUpdate = false) {
          for (var i in tags) {

                  // eslint-disable-next-line no-loop-func
              Tag.findOne({rawname:tags[i]}, function(errtag, result){    // Pour chaque tag dans la liste des tags en paramètre
                if (errtag) return console.error(errtag);

                  if (!result) {  // Si il n'y a pas de résultats en base
                      var newTag = new Tag({rawname:tags[i]});  // Un nouveau tag est crée et sauvegardé
                      newTag.save(function(err, tag){

                          if (err) return console.error(err);
                          Tag.updateOne({_id:tag.id}, {$push: {articles : articleid}}, function(err,res){
                              if (err) return console.error(err);                                         // On associe l'article au nouveau tag crée
                          });                                                                             // Et le nouveau tag, à l'article crée ou modifié
                          Article.updateOne({_id:articleid}, {$push: {tags : tag.id}}, function(err,res){
                              if (err) return console.error(err);
                          });
                      });
                  }
                  else { // Un résultat à été trouvé en base pour le tag actuel
                      if(isUpdate === false) { // Si cette fonction est appelée à la création d'un article
                          Tag.updateOne({_id:result._id}, {$push: {articles : articleid}}, function(err,res){
                              if (err) return console.error(err);                                             // On associe l'article au nouveau tag crée
                          });                                                                                 // Et le nouveau tag, à l'article crée ou modifié
                          Article.updateOne({_id:articleid}, {$push: {tags : result._id}}, function(err,res){
                              if (err) return console.error(err);
                          });
                      }
                      else { // Si il s'agit d'une modification d'article 
                          Article.findOne({_id:articleid}, function(err, article){    // On recherche les infos de l'article
                              if (err) return console.error(err);
                              var inArray = article.tags.some(function (articleTag) { // On checke sa liste de tags et on cherche s'il y a une occurence du tag actuel
                                  return articleTag.equals(result._id);
                              });
                            
                              if (inArray === false) { // Pas d'occurence, on fait le process d'association tag/article
                                  Tag.updateOne({_id:result._id}, {$push: {articles : articleid}}, function(err,res){
                                      if (err) return console.error(err);
                                  });
                                  Article.updateOne({_id:articleid}, {$push: {tags : result._id}}, function(err,res){
                                      if (err) return console.error(err);
                                  });
                              }
                          });
                      }
                  }
              });
          }
      }

      exports.getCategories = async function() {
          var categories;
          await Categorie.find({}, function(err, result){
            if (err) return console.error(err);
            categories = result;
          });

          return categories;
      }

      exports.getOneCategorie = async function(id) {
        var categorie;
        await Categorie.findOne({_id:id}, function(err, result){
          if (err) return console.error(err);
          categorie = result;
        });

        return categorie;
    }

      exports.getArticles = async function(){
          var articles;
          await Article.find({}, function(err, result){
            if (err) return console.error(err);
            articles = result;
          });
          return articles;
      }

      exports.getOneArticle = async function(id){
          var article;
          var tags;

          await Article.findById(id, function(err, result){
            if (err) return console.error(err);
            article = result;
          });

          await Article.findById(id).populate("tags", function(err, result){
            if (err) return console.error(err);
            tags = result;
          });

          for (var tag in tags) {
            var unTag = tags[tag].toJSON();
            delete unTag['articles'];
            tags[tag] = JSON.stringify(unTag);
          }

          article = article.toJSON();
          delete article['tags'];
          article['tags'] = tags;

          return article;
      }

      exports.getArticlesParCat = async function(id){
          var lesArticles = [];

          await Article.find({id_categorie:id}, function(err, result){
              if (err) return console.error(err);
              lesArticles = result;
          });

          var artTags = [];

          for (var article in lesArticles) {
              // eslint-disable-next-line no-loop-func
              await Article.findById(lesArticles[article]._id).populate("tags", function(err, lesTags){
                if (err) return console.error(err);
                artTags = lesTags;
              })

              for(var tag in artTags) {
                var unTag = artTags[tag].toJSON();
                delete unTag['articles'];
                artTags[tag] = JSON.stringify(unTag);
              }  
            
              var unArticle = lesArticles[article].toJSON();
              delete unArticle['tags'];
              unArticle['tags'] = artTags;
              lesArticles[article] = unArticle;

              artTags = [];
          }
          return lesArticles;
      }

      exports.getArticlesParTag = async function(tagid) {
        var articlesid;
        var articles = [];

        await Tag.findOne({_id:tagid}, 'articles', function(err, result){
            articlesid = result.articles;
        });

        for (var i in articlesid){
          await Article.findOne({_id:articlesid[i]}, function(err, article){
            var unArticle = article.toJSON();
            delete unArticle['tags'];
            delete unArticle['__v'];
            articles.push(unArticle);
          })
        }
        return articles;
      }

      exports.getArticlesParTitre = async function(titre) {
        var articles;

        await Article.find({titre:titre}, function(err, resArticles){
            articles = resArticles;
        });

        for (var i in articles) {
          var unArticle = articles[i].toJSON();
          delete unArticle['tags'];
          articles[i] = unArticle;
        }

        return articles;
      }

      exports.addArticle = function (categoryid, titre, content, tags = null) {
          var newArticle = new Article({contenu:content, titre:titre, id_categorie:categoryid});
          newArticle.save(function (err, article){
              if (err) return console.error(err);

              if (tags !== null) {
                manageTags(article.id, tags);
              }
          });
      }

      exports.updateArticle = function(id, titre, content, categorie, tags = null) {
          Article.updateOne({_id:id}, {titre: titre, contenu:content, id_categorie:categorie}, function(err,res){
              if (err) return console.error(err);
          });

          if (tags !== null) {
            manageTags(id, tags, true);
          }
      }

      exports.deleteArticle = function(id){
          Article.deleteOne({_id:id}, function(err){
              if (err) return console.error(err);
          });

          Tag.updateMany({}, {$pull: {articles : {$in : id}}}, function(err){
              if (err) return console.error(err);
          });
      }

      exports.getTags = async function(){
          var tags;
          await Tag.find({}, function(err, result){
            if (err) return console.error(err);
            tags = result;
          });
        
          return tags;
      }
  
      exports.getOneTag = async function(id){
          var tag;
          await Tag.findOne({_id:id}, function(err, result){
            if (err) return console.error(err);
            tag = result;
          });
        
          return tag;
      }

      // Ne supprime pas réellement un tag, cette fonction dissocie un tag d'un article.
      // N'étant donc pas réellement une suppression mais plutot une modification, la route est en PUT.
      exports.deleteTag = function(id, idtag){
          Article.updateOne({_id:id}, {$pull: {tags : {$in : idtag}}}, function(err){
            if (err) return console.error(err);
          });

          Tag.updateOne({_id:idtag}, {$pull: {articles : {$in : id}}}, function(err){
            if (err) return console.error(err);
          });
      }
  })  

  .catch( (err) => {
      console.error(`Error connecting to the database. \n${err}`);
  })