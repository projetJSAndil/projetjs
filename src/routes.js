const http = require('http')
const express = require('express')
const hostname = 'localhost';
const port = 4000;
const app = express();
const server = require('./server.js');
const cors = require('cors');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(__dirname + '/public'));
app.use(cors());

/* --- ROUTES --- */

app.get("/categories", function (req, res) {
    var categories = Promise.resolve(server.getCategories());
    categories.then((result) => {
        res.json(result);
    });
});

app.get("/category/:id", function (req, res) {
    var categories = Promise.resolve(server.getOneCategorie(req.params.id));
    categories.then((result) => {
        res.json(result);
    });
});

app.get("/articles", function (req, res) {
    var articles = Promise.resolve(server.getArticles());
    articles.then((result) => {
        console.log(result);
        res.json(result);
    });
});

app.get("/article/:id", function (req, res) {
    var article = Promise.resolve(server.getOneArticle(req.params.id));
    article.then((result) => {
        console.log(result);
        res.json(result);
    });
});

app.get("/articles/category/:id", function (req, res){
    var articles = Promise.resolve(server.getArticlesParCat(req.params.id));
    articles.then((result) => {
        console.log(result);
        res.json(result);
    })
});

app.get("/articles/tag/:id", function(req, res){
    var articles = Promise.resolve(server.getArticlesParTag(req.params.id));
    articles.then((result) => {
        console.log(result);
        res.json(result);
    })
});

app.get("/articles/titre/:titre", function(req, res){
    var titre = req.params.titre.replace("_", " ");
    var articles = Promise.resolve(server.getArticlesParTitre(titre));
    articles.then((result) => {
        console.log(result);
        res.json(result);
    })
});

app.post("/article/add", function(req, res){
    server.addArticle(req.body.categorie, req.body.titre, req.body.contenu, req.body.tags);
    res.json({result: 'Ajout effectué !'});
});

app.put("/article/update/:id", function (req, res) {
    server.updateArticle(req.params.id, req.body.titre, req.body.content, req.body.categ, req.body.tags);
    res.json({result: 'Modification effectuée !'});
});

app.delete("/article/delete/:id", function (req, res) {
    server.deleteArticle(req.params.id);
    res.json({result: 'Suppression effectuée !'});
});

app.get("/tags", function (req, res) {
    var tags = Promise.resolve(server.getTags());
    tags.then((result) => {
        console.log(result);
        res.json(result);
    });
});

app.get("/tag/:id", function (req, res) {
    var tag = Promise.resolve(server.getOneTag(req.params.id));
    tag.then((result) => {
        console.log(result);
        res.json(result);
    });
});

app.put("/article/:id/deletetag/:idtag", function (req, res) {
    server.deleteTag(req.params.id, req.params.idtag);
    res.json({result: 'Suppression effectuée !'});
});

const serveur = http.createServer(app);
serveur.listen(port, hostname, function () {
    console.log('Le serveur tourne sur l\'adresse : http://' + hostname + ':' + port);
});