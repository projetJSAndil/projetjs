## Comment mettre en marche l'application ? ##

Après avoir téléchargé le répertoire du projet, rendez vous sur un terminal quelconque et déplacez vous au chemin de destination du projet sur votre ordinateur.

Il faudra reinstaller tous les paquets nécessaires au fonctionnement de l'application.
Pour cela, tapez dans votre terminal la commande `npm install`

Une fois le téléchargement terminé (un fichier "node_modules" devrait s'être rajouté à la racine du projet), pour lancer l'application, lancez sur votre terminal (en veillant à toujours vous trouver à la racine) : 

1. *node server/routes.js* pour mettre en route le serveur et la liaison à la base MongoDB
2. `sur un autre terminal (toujours à la racine du projet)` *npm start* pour mettre en route l'interface React

`Cependant`, le projet en l'étât actuel est `non fonctionnel`, l'interface en React ne permet pas de faire fonctionner l'application.

Vous trouverez plus bas dans la partie "Documentation API" un explicatif pour faire tourner manuellement les fonctionnalités dévelopées (un aperçu des données renvoyées sera présent dans le terminal côté serveur/liaison MongoDB).

## Descriptif général ##

Ce projet à pour but de mettre en place une interface type "wiki", permettant une gestion et une recherche d'articles rédigés à la main, en gérant à la fois le développement orienté back-end et front-end.
Le livrable doit etre fourni avec un certain nombre de fonctionnalités requises pour un fonctionnement "minimal".

### Il doit être possible de  : ###

- Créer / Modifier / Supprimer un article
- Ajouter un ou plusieurs tags à un article
- Effectuer une recherche par catégorie d'article, par tags ajoutés ou par titre.
- Implanter un versionning des articles, pouvoir consulter chaque version modifiée d'un article.

Le développement se scinde en deux parties majeures : 
- un développement orienté back-end sous Node.Js avec une liaison à une base cloud MongoDB.
- un développement orienté front-end sous React JS avec une communication vers la REST API      niveau back-end.

## Fonctionnalités développées ##

Pour rappel, le projet est à ce jour malheureusement `non fonctionnel`.
Il dispose toutefois de toutes les fonctionnalités C.R.U.D basiques ainsi que de tous les systèmes de recherche d'articles en back.

Les routes et leurs fonctionnalités ont été organisées sous forme d'une `REST API` NodeJS et la liaison à la base de données s'effectue directement vers la plateforme `Cloud Atlas` de MongoDB.

### Sont donc implantés : ### 

- La récupération de tous les articles `/articles`
    *-> getArticles() : renvoie les articles sous forme d'objets JSON* 

- La recherche d'articles :
    - par catégorie `/articles/category/:id`
        *-> getArticlesParCat(idCategorie) : renvoie un ou plusieurs articles selon la catégorie choisie*
    - par tag `/articles/tag/:id)`
        *-> getArticlesParTag(idTag) : renvoie un ou plusieurs articles affectés par le tag choisi*
    - par titre `/articles/titre/:titre` 
        *-> getArticlesParTitre(libelle) : renvoie un (voire plusieurs) articles ayant le titre recherché*


- L'ajout d'un article `/article/add`
    *-> addArticle(titre, categorie, contenu, tags) : ajoute un nouvel article en base, ainsi que les tags qui lui sont associés si ceux ci ne sont pas déja existants. Crée une association article/tag.*
- La modification d'un article `/article/update/:id`
    *-> updateArticle(id, titre, categorie, contenu, tags) : modifie un article existant, permet également de lui rajouter un ou plusieurs tags a condition que ceux si ne soient pas déjà associés à l'article.*
- La suppression d'un article `/article/delete/:id`
    *-> deleteArticle(id) : supprime un article de la base, le dissociant également des tags qui lui étaient greffés.*
- La dissociation d'un tag à un article `/article/:idarticle/deletetag/:idtag`
    *-> deleteTag(idArticle, idTag) : supprime le tag choisi de la liste des tags de l'article, et vice versa.*

## Documentation API ##

Pour faire fonctionner manuellement l'application, il vous faudra lancer l'application `Postman` sur votre machine. (https://www.postman.com/downloads/)

Le serveur fonctionne sur l'adresse `http://localhost:4000̀` toutes vos routes devront être préfixées de la sorte.

Comme vu précedemment dans la partie *Fonctionnalités développées*, il vous est possible de requêter un certain nombre de routes (la liste de toutes les routes disponibles vous sera affichée plus bas).

### Petites particularités ###

Certaines routes ont des spécificités : 

- `http://localhost:4000̀/articles/add` / `http://localhost:4000̀/articles/update/:id`
    -> pour mener à  bien le fonctionnement de ces routes, il vous faudra dans un premier temps vous positionner sur le bon protocole de requêtage : `POST` pour la première route et `PUT` pour la seconde.

    -> tous les "id" à renseigner sont des `ObjectId` générés par MongoDB à la création d'une occurence en base (ils sont de la forme "60e4032ea6fe1b23ead9b155"). 
    Pour votre convénience, des éléments de tests seront stockés par défaut pour référence.
    Vous pourrez en retrouver en requêtant */articles* */tags* ou */categories* par exemple.

    -> des données sont à faire passer au serveur, pour ça il vous faut les renseigner au format *JSON* dans le sous menu `Body > raw -> JSON` sous cette forme : 

    ```JSON
    {
        "category" : ObjectID, 
        "titre" : "Titre article",
        "content" : "Contenu article",
        "tags": ["tag1","tag2", "tag3"]
    }
    ```

    -> au requêtage de ces deux routes (de même que pour la suppression), vous aurez un message de confirmation affiché dans votre console après envoi.

- `http://localhost:4000̀/article/category/:id` / `http://localhost:4000̀/articles/titre/:titre`
     
    Lors du développement, à l'appel de ces routes, il a été décelé une erreur de récupération de   certaines données.
    Ceci est lié à la déclaration de fonctions asynchrones demandées par les fonctions de requêtage de `mongoose`. N'ayant pas pu pleinement comprendre leur fonctionnement, il n'a pas été possible de corriger ces erreurs.

    -> La première route peut renvoyer une liste de tags vide pour chaque article après avoir rafraîchi un envoi de données. Voire complêtement retourner un ou plusieurs objets indéfinis, qui pourtant étaient bel et bien affichés lors d'une première requête sur cette route.  

    -> De même que pour la route précédente, dès lors qu'il s'agit d'afficher plusieurs articles ayant des spéficicités de recherches (ici par tags ou par catégories), certains objets peuvent devenir "indéfinis" si la demande est réitérée.

### Table de routage ###

- GET | /categories | projetjs/src/server.js : getCategories()            
- GET |   /category/:id | projetjs/src/server.js : getOneCategorie(id)        
- GET |    /articles | projetjs/src/server.js : getArticles()              
- GET |    /articles/:id | projetjs/src/server.js : getOneArticle(id)          
- GET |    /articles/category/:id | projetjs/src/server.js : getArticlesParCat(id)      
- GET |    /articles/tags/:id | projetjs/src/server.js : getArticlesParTag(id)      
- GET |    /articles/titre/:titre | projetjs/src/server.js : getArticlesParTitre(titre)  
- GET |    /tags | projetjs/src/server.js : getTags(id)                
- GET |    /tag/:id | projetjs/src/server.js : getOneTag(id)      
- PUT |    /article/:id/deletetag/:idtag | projetjs/src/server.js : deleteTag(idarticle, idtag)      
- PUT |     /article/update/:id | projetjs/src/server.js : updateArticle(id)          
- POST |   /articles/add | projetjs/src/server.js : addArticle()               
- DELETE |  /article/delete/:id | projetjs/src/server.js : deleteArticle(id)          